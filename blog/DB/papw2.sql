use papaw2;

create table Usuario (
Uid int primary key auto_increment,
nombre varchar(45),
apellido1 varchar(45),
apellido2 VARCHAR(45),
email varchar(45) unique not null,
usuario varchar(45) unique not null,
pasword varchar(45) not null,
fecha DATETIME
);

create table imagenes (
Iid int primary key auto_increment,
ubicacion varchar(255),
img BLOB,
IUid int  not  null,
Index idUsiaro (IUid),
foreign key (IUid) 
references Usuario(Uid)
ON DELETE CASCADE
);

create table Post (
Pid int primary key auto_increment,
Pnombre varchar(45),
Pdescripcion varchar(255),
PUid int  not  null,
likes float,
dislikes float,
Index idUsiaro (PUid),
foreign key (PUid) 
references Usuario(Uid)
ON DELETE CASCADE
);


create table multimedia (
Mid int primary key auto_increment,
ubicacion varchar(255),
img BLOB,
MPid int  not  null,
Index idPost (MPid),
foreign key (MPid) 
references Post(Pid)
ON DELETE CASCADE
);

create table Conversacion (
Cid int primary key auto_increment,
CUid1 int  not  null,
CUid2 int  not  null,
Index idUsuario1 (CUid1),
foreign key (CUid1) 
references Usuario(Uid)
ON DELETE CASCADE,
Index idUsuario2 (CUid2),
foreign key (CUid2) 
references Usuario(Uid)
ON DELETE CASCADE
);

create table mensaje (
Msjid int primary key auto_increment,
Msj varchar(255),
Msjdate DATETIME,
MsjCid int  not  null,
MsjUid int  not  null,
Index idConversacion (MsjCid),
foreign key (MsjCid) 
references Conversacion(Cid)
ON DELETE CASCADE,
Index msjidUsuario (MsjUid),
foreign key (MsjUid) 
references Usuario(Uid)
ON DELETE CASCADE
);

create table comentario (
Comid int primary key auto_increment,
Com varchar(255),
Comdate DATETIME,
ComPid int  not  null,
ComUid int  not  null,
Index ComidPost (ComPid),
foreign key (ComPid) 
references Post(Pid)
ON DELETE CASCADE,
Index ComidUsuario (ComUid),
foreign key (ComUid) 
references Usuario(Uid)
ON DELETE CASCADE
);

drop TABLE multimedia
