<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function dashboard()
    {
        $posts = App\Models\Post::where('status', '=', 'publicado')
        ->orderby('created_at', 'desc')
        ->paginate(3);
        $usuario = App\Models\User::all();
        return view('dashboard', compact('posts', 'usuario'));
    }
    public function paginacion()
    {
        $posts = App\Models\Post::where('status', '=', 'publicado')
        ->orderby('created_at', 'desc')
        ->paginate(3);
        $usuario = App\Models\User::all();
        return view('post.pagina', compact('posts', 'usuario'));
    }
    public function content($id)
    {
        $post = App\Models\Post::findOrFail($id);
        $usuarioP = App\Models\User::all();
        return view('content', compact('post', 'usuarioP'));
    }
    public function perfil($id)
    {
        $user = App\Models\User::findOrFail($id);
        $Userposts = App\Models\Post::all();
        return view('perfil', compact('user', 'Userposts'));
    }
    public function miperfil()
    {
        return view('miperfil');
    }
    public function crear(Request $request)
    {
        //return $request->all();

        switch($request->submitbutton) {

            case 'publicar':
                $postNuevo = new App\Models\Post;
                $postNuevo->nombre = $request->nombre;
                $postNuevo->descripcion = $request->descripcion; 
                $postNuevo->status = "publicado";
                $postNuevo->usuario = auth()->user()->email;
                $postNuevo->imagen1 = file_get_contents($request->imagen1);
                $postNuevo->imagen2 = file_get_contents($request->imagen2); 
                $postNuevo->save();

        return back()->with('mensaje', 'Post Agregado!');
            break;

            case 'borrador':
                $postNuevo = new App\Models\Post;
                $postNuevo->nombre = $request->nombre;
                $postNuevo->descripcion = $request->descripcion; 
                $postNuevo->status = "borrador";
                $postNuevo->usuario = auth()->user()->email;
                $postNuevo->imagen1 = file_get_contents($request->imagen1);
                $postNuevo->imagen2 = file_get_contents($request->imagen2); 
                $postNuevo->save();

                return back()->with('mensaje', 'Post Agregado!');
            break;

        }
    }
    public function avatar(Request $avatar)
    {
        return $request->all();
    }
}
