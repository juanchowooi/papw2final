@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/content.css')}}">
    <link rel="stylesheet" href="{{asset('css/vote.css')}}">
@endsection

@section('contenido')
    <div class="c-container">
        <div class="c-post">
            <img class="c-img" src="img/perfil/1.png">
            <div class="c-titulo">
                POST1 (borrador)
            </div>
            <div class="c-sub">
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.
            </div>
            <div class="c-leavecomments">
            </div>
            <div class="c-comments">
                <hr>
                <hr>
            </div>
        </div>
        <div class="c-info">
            <div class="c-user">
                <img class="c-avatarI" src="img/avatar.jpg">
                <h2 class="c-usuariotxt">MI USUARIO</h2>
                <button class="c-btnF">Editar</button>
                <button class="c-btnF">Postear </button>
                <button class="c-btnEl">Borrar <i class = "fas fa-trash "></i></button>
            </div>
        </div>
    </div>

    <div class="c-otros">
        <ul class="p-ulO">
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/1.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/2.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/3.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/4.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/5.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/6.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/7.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/8.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/9.png">
                </div>
            </li>
        </ul>
    </div>
@endsection