@extends('layouts.base')

@section('title', 'PERFIL')



@section('P-css')
    <link rel="stylesheet" href="{{asset('css/banner.css')}}">
@endsection


@section('perfil')

<div class="p-conn">
<div class="p-container">
    <div class="p-main">
        <ul class="p-ul">
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/2.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST2</a>  
                </div>
            </li>
            <li class="p-li">
                <a href="http://127.0.0.1:8000/content">
                    <div class="p-div">
                        <img class="p-img" src="img/perfil/3.png">
                    </div>
                    <div class="p-posttitulo">
                        <a>POST3</a>
                    </div>
                </a>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/4.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST4</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/5.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST5</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/6.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST6</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/7.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST7</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/8.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST8</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/9.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST9</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/10.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST10</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/11.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST11</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/12.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST12</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/13.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST13</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/14.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST14</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/15.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST15</a>  
                </div>
            </li>
        </ul>
    </div>
</div>
<div  class="p-friends">
    <div class = "p-busqueda">
        <div class ="p-filtro">
            <input class ="p-check" type="checkbox" id="filtro1" name="filro1" value="personaje">
            <label class ="p-tag" for="filtro1">Personaje</label><br>
            <input class ="p-check" type="checkbox" id="filtro2" name="filro2" value="Escena">
            <label class ="p-tag" for="filtro2"> Escena</label><br>
            <input class ="p-check" type="checkbox" id="filtro3" name="filro3" value="Dibujo">
            <label class ="p-tag" for="filtro1">Dibujo</label><br>
            <input class ="p-check" type="checkbox" id="filtro4" name="filro4" value="Foto">
            <label class ="p-tag" for="filtro2"> Foto</label><br>
            <input class ="p-check" type="checkbox" id="filtro5" name="filro5" value="3D">
            <label class ="p-tag" for="filtro1">3D</label><br>
    </div>
    </div>
</div>
</div>
    
@endsection