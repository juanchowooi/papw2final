@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/content.css')}}">
    <link rel="stylesheet" href="{{asset('css/vote.css')}}">
@endsection

@section('contenido')
    <div class="c-container">
        <div class="c-post">
            <img class="c-img" src="data:{{$post->mime}};base64, {{ base64_encode($post->imagen1) }}">     
            @if ($post->imagen2 != null)
                <img class="c-img" src="data:{{$post->mime}};base64, {{ base64_encode($post->imagen2) }}">
            @endif
            <div class="c-titulo">
                {{ $post->nombre }}
            </div>
            <div class="c-sub">
                {{ $post->descripcion }}
            </div>
            <div class="c-leavecomments">
                <img class="c-avatar" src="img/avatar/avatar3.png">
                <input class="c-cometario" type="text" name="Comentario">
                <button class="c-btncmnt">ENVIAR</button>
            </div>
            <div class="c-comments">
                <hr>
                <div class="c-comment">
                    <img class="c-avatar" src="img/avatar/avatar4.png">
                    <div class="c-datos">
                        <h2 class="c-miusuario">MI USUARIO</h2>
                        <h3 class="c-micomentario">aqui esta mi comentario!!!</h3>
                        <h4 class="c-date">2/22</h4>
                    </div>
                </div>
                <div class="c-comment">
                    <img class="c-avatar" src="img/avatar/avatar5.jpg">
                    <div class="c-datos">
                        <h2 class="c-miusuario">MI USUARIO</h2>
                        <h3 class="c-micomentario">Saludos crack!!!</h3>
                        <h4 class="c-date">2/22</h4>
                    </div>
                </div>
                <hr>
            </div>
        </div>
        @foreach ($usuarioP as $UserP)
        @if ($post->usuario == $UserP->email)
        <div class="c-info">
            <div class="c-user">
                <a href="{{ route('perfil', $UserP) }}">
                <img class="c-avatarI" src="data:image/png;base64, {{ base64_encode($UserP->avatar) }}" onerror="this.onerror=null; this.src='img/default.png'">
                </a>
            <h2 class="c-usuariotxt"> {{ $UserP->name }}</h2>
                <button class="c-btnF">SEGUIR</button>
            </div>
        </div>
        @endif
        @endforeach
    </div>

    <div class="c-otros">
        <ul class="p-ulO">
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/1.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/2.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/3.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/4.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/5.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/6.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/7.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/8.png">
                </div>
            </li>
            <li class="p-liO">
                <div class="muestra">
                    <img class="c-otrosIMG" src="img/perfil/9.png">
                </div>
            </li>
        </ul>
    </div>

    <div class="vote">
        <div class="upvote">
            <i class="fas fa-sort-up"></i>
            <input class="upvoteN" type="number" value="12">
        </div>
        <div class="downvote">
            <i class="fas fa-sort-down"></i>
            <input class="downvoteN" type="number" value="2">
        </div>
    </div>
@endsection