@extends('layouts.base')

@section('title', 'Pagina | Cuenta')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection


@section('cuenta')
<div class="container">
    <div class="main">
    <article>
            <div class="wraper">
                <section class="post">
                            <div class="caption">
                                <div class = "row">
                                    <div class="col-25">
                                    <label></label>
                                    </div>
                                    <div class="col-75">
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                    <img class="cuentaAvatar" src="img/avatar.jpg">
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Usuario:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>Usuario123</label>
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Correo:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>email@yahoo.com</label>
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Contraseña:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>*************</label>
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Titulo:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>MI BLOG PERSONAL</label>
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Descripcion:</label>
                                    </div>
                                    <div class="col-75">
                                    <label>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
</label>
                                    </div>
                                    <div class="col-5">
                                    <label color: blue;>Editar</label>
                                    </div>
                                </div>
                                <div class = "linea"></div>
                                <div class = "row">
                                    <div class="col-25">
                                    <label>Banner:</label>
                                    </div>
                                    <div class="col-75">
                                    </div>
                                    <div class="col-5">
                                    <label>Editar</label>
                                    </div>
                                    <img class="baner" src="img/banner.png">
                                </div>
                                <div class = "linea"></div>
                            </div>                       
                </section>
            </div>
        </article>
        </div>
</div>
@endsection