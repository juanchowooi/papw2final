
@extends('layouts.base')

@section('title', 'HOME PAGE')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('content')
    <img src="img/img.png">
@endsection

@section('container')


<div class="container">
    <div class="main" id="contennd">

        @include('post.pagina')

    </div>

</div>

<script>
    let pagina = 2
    window.onscroll = () =>{
        if((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight ) {
            fetch('/dashboard/paginacion?page='+pagina,{
                method:'get'
            })
            .then(response => response.text() )
            .then(html => {
                document.getElementById("contennd").innerHTML += html
                pagina++;
            })
            .cath(error => console.log(error))
        }
    }
</script>

@endsection

