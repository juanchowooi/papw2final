@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/banner.css')}}">
@endsection

@section('banner')

    <div class="banner">
        <div class="img-wraper">
            <img class="img-banner" src="img/banner.png">
        </div>
        <div class="blog=titulo">
            <figure class="p-figure">
                <img class="user-avatar" src="img/avatar.jpg">
            </figure>
        </div>
        <div class="tittle">
            <h1 class="p-titulo">MI BLOG PERSONAL</h1>
        </div>
        <div class="descrippcion">
            <h3 class="p-descripcion">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h3>
        </div>
    </div>
@endsection

@section('perfil')

<div class="p-conn">
<div class="p-container">
    <div class="p-main">
        <ul class="p-ul">
            <li class="p-li">
            <a href="http://127.0.0.1:8000/borrador">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/1.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST1</a>
                    <a><i class="fas fa-eye-slash"></i></a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/2.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST2</a>  
                </div>
            </li>
            <li class="p-li">
                <a href="http://127.0.0.1:8000/content">
                    <div class="p-div">
                        <img class="p-img" src="img/perfil/3.png">
                    </div>
                    <div class="p-posttitulo">
                        <a>POST3</a>
                        <a><i class="fas fa-eye"></i></a>  
                    </div>
                </a>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/4.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST4</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/5.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST5</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/6.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST6</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/7.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST7</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/8.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST8</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/9.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST9</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/10.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST10</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/11.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST11</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/12.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST12</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/13.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST13</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/14.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST14</a>  
                </div>
            </li>
            <li class="p-li">
                <div class="p-div">
                    <img class="p-img" src="img/perfil/15.png">
                </div>
                <div class="p-posttitulo">
                    <a>POST15</a>  
                </div>
            </li>
        </ul>
    </div>
</div>
<div  class="p-friends">
    <h1 class="txtFriends">FRIENDS</h1>
    <ul class="p-ulF">
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar2.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar3.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar4.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar5.jpg">
            </div>
        </li>
    </ul>
</div>
<div class="FbtnMbtn">
    <button class="btnFollow">SEGUIR</button>
    <a href="http://127.0.0.1:8000/message">
        <button class="btnFollow">MENSAJE</button>
    </a>        
</div>
</div>
    
@endsection