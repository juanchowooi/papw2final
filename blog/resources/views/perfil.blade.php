@extends('layouts.base')

@section('title', 'PERFIL')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection

@section('P-css')
    <link rel="stylesheet" href="{{asset('css/banner.css')}}">
@endsection

@section('banner')

    <div class="banner">
        <div class="img-wraper">
            <img class="img-banner" src="data:image/png;base64, {{ base64_encode($user->banner) }}" onerror="this.onerror=null; this.src='img/default.png'">
        </div>
        <div class="blog=titulo">
            <figure class="p-figure">
                <img class="user-avatar" src="data:image/png;base64, {{ base64_encode($user->avatar) }}">
            </figure>
        </div>
        <div class="tittle">
            <h1 class="p-titulo">MI BLOG PERSONAL</h1>
        </div>
        <div class="descrippcion">
            <h3 class="p-descripcion">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h3>
        </div>
    </div>
@endsection

@section('perfil')

<div class="p-conn">
<div class="p-container">
    <div class="p-main">
        <ul class="p-ul">
            @foreach ($Userposts as $Useritem)
            @if ($user->email == $Useritem->usuario)
                <li class="p-li">
                <a href="{{ route('content', $Useritem) }}">
                    <div class="p-div">
                     <img class="p-img" src="data:image/png;base64, {{ base64_encode($Useritem->imagen1) }}">
                    </div>
                </a>
                    <div class="p-posttitulo">
                    <h2>{{$Useritem->nombre}}</h2>
                        <a><i class="fas fa-eye-slash"></i></a>  
                 </div>
                </li>
            @endif
            @endforeach
        </ul>
    </div>
</div>
<div  class="p-friends">
    <h1 class="txtFriends">FRIENDS</h1>
    <ul class="p-ulF">
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar2.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar3.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar4.png">
            </div>
        </li>
        <li class="p-liF">
            <div class="p-friend">
                <img class="p-imgF" src="img/avatar/avatar5.jpg">
            </div>
        </li>
    </ul>
</div>
<div class="FbtnMbtn">
    <button class="btnFollow">SEGUIR</button>
    <a href="http://127.0.0.1:8000/message">
        <button class="btnFollow">MENSAJE</button>
    </a>        
</div>
</div>
    
@endsection