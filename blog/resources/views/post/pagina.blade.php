@foreach ($posts as $item)
           
<article>
    @foreach ($usuario as $u)
        
    @if ($u->email == $item->usuario)
    <div class="avatar">
        <a href="{{ route('perfil', $u) }}">
            <img class="avatarimg" src="data:image/png;base64, {{ base64_encode($u->avatar) }}">
        </a>
    </div>
    @endif

    @endforeach
    <div class="post_wraper">
        <section class="post">
            <figure class="post-content" data-photo-width="500">
                <div class="photo-wraper">
                    <a href="{{ route('content', $item) }}">
                        <div class="photo-wraper-inner">
                            <img class="img" src="data:image/png;base64, {{ base64_encode($item->imagen1) }}">     
                        @if ($item->imagen2 != null)
                            <img class="img" src="data:image/png;base64, {{ base64_encode($item->imagen2) }}">
                        @endif
                        </div>
                    </a>
                        <div class="caption">
                            <h2>{{$item->nombre}}</h2>
                            <p>{{$item->descripcion}}</p>        
                        </div>
                </div>
            </figure>
        </section>
        <section class="panel">
            <footer class="post-footer">
                    <section class="post-control">
                        <button class="Cbutton" id="likebtn">
                            <i class="fa fa-thumbs-up"></i>
                        </button>
                        <input class="Cinput" type="number" id="input1" value="0" name="">
                        <button class="Cbutton" id="dislikebtn">
                            <i class="fa fa-thumbs-down"></i>
                        </button>
                        <input class="Cinput" type="number" id="input2" value="0" name="">
                    </section>
            </footer>
        </section>
    </div>
</article>

@endforeach()