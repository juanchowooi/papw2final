@extends('layouts.base')

@section('title', 'Pagina | Cuenta')

@section('sidebar')
    @parent
    <p>This is appended to the master sidebar</p>
@endsection


@section('publicar')

@if (session('mensaje'))
    <div class="alert alert-success">
        {{ session('mensaje') }}
    </div>
@endif


<img class = "center" id= "images"  src="">

<div class="container">
    <div class="main">
    <article>
            <div class="wraper">
            
                <section class="post">
                
                            <div class="caption">
                            
                            <form action="{{ route('post.crear') }}" name="publicar" method="POST" enctype="multipart/form-data">
                                @csrf
                                <div class = "row">
                                        <div class="col-25">
                                        <label>Archivo:</label>
                                        </div>
                                        <div class="col-75">
                                        <input class = "text" type="file" id="myFile" name="imagen1" onchange = "readURL(this);">
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class="col-25">
                                        <label>Archivo:</label>
                                        </div>
                                        <div class="col-75">
                                        <input class = "text" type="file" id="myFile" name="imagen2" onchange = "readURL(this);">
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class="col-25">
                                        <label>Titulo:</label>
                                        </div>
                                        <div class="col-75">
                                        <input class = "texto" type="text" id="fname" name="nombre" placeholder="Your name.." >
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <div class="col-25">
                                        <label>Descipcion:</label>
                                        </div>
                                        <div class="col-75">
                                        <textarea class = "texto" id="subject" name="descripcion" placeholder="Write something.." style="height:100px"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-25">
                                        <label>Tag:</label>
                                        </div>
                                        <div class="col-75">
                                        <input class ="p-check" type="checkbox" id="filtro1" name="filro1" value="personaje">
                                        <label class ="p-tag" for="filtro1">Personaje</label><br>
                                        <input class ="p-check" type="checkbox" id="filtro2" name="filro2" value="Escena">
                                        <label class ="p-tag" for="filtro2"> Escena</label><br>
                                        <input class ="p-check" type="checkbox" id="filtro3" name="filro3" value="Dibujo">
                                        <label class ="p-tag" for="filtro1">Dibujo</label><br>
                                        <input class ="p-check" type="checkbox" id="filtro4" name="filro4" value="Foto">
                                        <label class ="p-tag" for="filtro2"> Foto</label><br>
                                        <input class ="p-check" type="checkbox" id="filtro5" name="filro5" value="3D">
                                        <label class ="p-tag" for="filtro1">3D</label><br>
                                        </div>
                                    </div>
                                    <div class = "row">
                                        <input type="submit" value="publicar" name="submitbutton"> 
                                        <input type="submit" value="borrador" name="submitbutton">     
                                    </div>
                                </form>
                            </div>                       
                </section>
            </div>
        </article>
        </div>
</div>

<script>
    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#images')
                .attr('src', e.target.result)
        };

        reader.readAsDataURL(input.files[0]);
    }
}
</script>
@endsection