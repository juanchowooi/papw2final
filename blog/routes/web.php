<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PageController::class, 'inicio'])->name('inicio');

//Route::get('/dashboard', [PageController::class, 'dashboard']);

Route::get('/cuenta', function () {
    return view('cuenta');
});

Route::get('/publicar', function () {
    return view('publicar');
});

Route::get('/message', function () {
    return view('message');
});

Route::get('/landing', function () {
    return view('landing');
});

Route::get('/borrador', function () {
    return view('borrador');
});


Route::get('/busqueda', function () {
    return view('busqueda');
});

Route::get('/prueba', function () {
    return view('prueba');
});
Auth::routes();

Route::get('/dashboard', [App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');

Route::get('/dashboard/paginacion', [App\Http\Controllers\HomeController::class, 'paginacion']);

Route::get('/post/{id}', [App\Http\Controllers\HomeController::class, 'content'])->name('content');

Route::post('/', [App\Http\Controllers\HomeController::class, 'crear'])->name('post.crear');

Route::get('/user/{id}', [App\Http\Controllers\HomeController::class, 'perfil'])->name('perfil');

Route::get('/user', [App\Http\Controllers\HomeController::class, 'miperfil'])->name('miperfil');